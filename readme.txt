To use this program, please provide one or two .txt file as arguments.

If two .txt files are supplied, then the first file will be used for comparing against each word in the second input file.

If one file is supplied, the first file will be used for comparing user entered queries to, to find the match score for the entered query when compared with all the 'words' in the provided .txt file.
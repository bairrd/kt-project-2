# Knowledge Technologies Project 1
# Tom Grundy - 541 802 - tgrundy@student.unimelb.edu.au

import os
import sys
import string
import re

generalStartupMessage = '''To switch between finding the edit score using a trie data structure and
using an edit distance algorithm, type :trie or :editDistance.

For a full list of commands, type :help'''

fullHelpText = ''':quit (or :q) - quits the program. 

:help (or :h) - Displays this help text. 

:trie (or :t) - Switches to searching user entered queries in a trie made from the required input text file.

:editDistance (or :e) - Switches to searching an edit distance table, based on the required input text file.

:memory (or :m) - Gives the current size of both the trie and edit distance data structures.

:printInput (or :p) - Prints the preprocessed version of the required input text file. 
All punctuation and capitals have been removed from this text.

:setErrorNum (or :s) - Lets the user set the amount of errors acceptable when traversing the trie.
User input should be an integer amount, and preferably less than or equal to 2.
Default value, if not set, is 0.
'''

trieModeMessage = "Now in trie mode." 
editDistanceModeMessage = "Now in edit distance mode."
setErrorNumMessage = "Enter the number of errors allowable when treversing a trie."


#http://stackoverflow.com/questions/1130992/most-efficient-data-structure-for-a-read-only-list-of-strings-about-100-000-wi

class TrieNode():
	def __init__(self, letter="", final=False):
		self.letter = letter
		self.children = {}
		self.final = final
	def __contains__(self, letter):
		return letter in self.children
	def get(self, letter):
		return self.children[letter]

	def add(self, letters, n=-1, index=0):
		if (n < 0):
			n = len(letters)
		if (index >= n):
			self.final = True
			return
		letter = letters[index]
		if letter in self.children:
			child = self.children[letter]
		else:
			child = TrieNode(letter)
			self.children[letter] = child
		child.add(letters, n, index+1)


def preprocessInputFile(filename):
	try:
		f = open(filename, "r")
		outText = f.read()
		outText = outText.lower().translate(string.maketrans("",""), string.punctuation)
		outText = re.sub(r'\s+',' ',outText)
		return outText
	except IOError as e:
		print "Please provide a valid filename."
		sys.exit(1)

def buildTrie(inputText):
	trie = TrieNode()
	for word in inputText.split(" "):
		trie.add(word)

	return trie

def buildEditDistanceArray(preprocessedText):
	editDistanceArray = []
	for word in preprocessedText.split(" "):
		editDistanceArray.append(word)

	return editDistanceArray

def trieTraverser(trie, trieSizeArray):
	for childLetter in trie.children:
		child = trie.children[childLetter]
		trieSizeArray.append(childLetter)
		trieTraverser(child, trieSizeArray)

def getDataStructureSize(trie, editDistanceArray):
	
	trieSizeArray = [""]
	editDistanceArraySize = 0

	trieTraverser(trie, trieSizeArray)

	editDistances = getSubstringAndMatchScore_editDistance("", editDistanceArray, True)

	print "The trie has " + str(len(trieSizeArray)) + " elements."
	print "The edit distance array has " + str(len(editDistances)) + " elements."



def findInTrie(trie, letters, lettersToFind, numOfErrorsAllowed, errorsSoFar, foundStrings, foundSet, letterIndex, lettersSoFar):

	if (errorsSoFar <= numOfErrorsAllowed):

		if (lettersToFind <= 0):
			if (len(trie.children.keys()) == 0):
				uniqueLettersSoFar = lettersSoFar  + str(errorsSoFar)
				if (uniqueLettersSoFar not in foundSet and letters in lettersSoFar):
					foundSet.add(uniqueLettersSoFar)
					foundStrings.append({"string": lettersSoFar, "errors":errorsSoFar})
			else:
				for childLetter in trie.children:
					child = trie.children[childLetter]
					getRemainingLetters(child, letters, lettersSoFar + child.letter, errorsSoFar, foundStrings, foundSet)


		else:

			# recurse through children. Below code currently matches on if the letters are in the string.
			# This needs to stop doing this recursion when the first letter is found or something
			# for childLetter in trie.children:
			# 	child = trie.children[childLetter]
			# 	findInTrie(child, letters, lettersToFind, numOfErrorsAllowed, errorsSoFar, foundStrings, foundSet, letterIndex, lettersSoFar + childLetter)


			letter = letters[letterIndex]

			if (letterIndex < numOfErrorsAllowed):
				for childLetter in trie.children:
					child = trie.children[childLetter]
					if (childLetter == letter):
						findInTrie(child, letters, lettersToFind - 1, numOfErrorsAllowed, errorsSoFar, foundStrings, foundSet, letterIndex + 1, lettersSoFar + childLetter)
					else:
						findInTrie(child, letters, lettersToFind - 1, numOfErrorsAllowed, errorsSoFar + 1, foundStrings, foundSet, letterIndex + 1, lettersSoFar + childLetter)

			elif (letter in trie.children):
				for childLetter in trie.children:
					child = trie.children[childLetter]
					if (childLetter == letter):
						findInTrie(child, letters, lettersToFind - 1, numOfErrorsAllowed, errorsSoFar, foundStrings, foundSet, letterIndex + 1, lettersSoFar + childLetter)
					else:
						findInTrie(child, letters, lettersToFind - 1, numOfErrorsAllowed, errorsSoFar + 1, foundStrings, foundSet, letterIndex + 1, lettersSoFar + childLetter)

			else:
				errorsSoFar += 1
				if (numOfErrorsAllowed - errorsSoFar >= 0):
					origLettersSoFar = lettersSoFar
					lettersToFind -= 1
					for childLetter in trie.children:
						lettersSoFar = origLettersSoFar + childLetter
						child = trie.children[childLetter]
						findInTrie(child, letters, lettersToFind, numOfErrorsAllowed, errorsSoFar, foundStrings, foundSet, letterIndex + 1, lettersSoFar)


def getRemainingLetters(trie, letters, lettersSoFar, errorsSoFar, foundStrings, foundSet):
	if (len(trie.children.keys()) == 0):
		uniqueLettersSoFar = lettersSoFar + str(errorsSoFar)

		if (uniqueLettersSoFar not in foundSet):
			foundSet.add(uniqueLettersSoFar)
			foundStrings.append({"string": lettersSoFar, "errors":errorsSoFar})
	else:
		origLettersSoFar = lettersSoFar
		for childLetter in trie.children:
			lettersSoFar = origLettersSoFar + childLetter
			child = trie.children[childLetter]
			getRemainingLetters(child, letters, lettersSoFar, errorsSoFar, foundStrings, foundSet)


def findInTrieRecurser(trie, letters, lettersToFind, numOfErrorsAllowed, errorsSoFar, foundStrings, foundSet, letterIndex, lettersSoFar):
	for childLetter in trie.children:
		child = trie.children[childLetter]
		findInTrie(child, letters, lettersToFind, numOfErrorsAllowed, errorsSoFar, foundStrings, foundSet, letterIndex, lettersSoFar + childLetter)
		findInTrieRecurser(child, letters, lettersToFind, numOfErrorsAllowed, errorsSoFar, foundStrings, foundSet, letterIndex, lettersSoFar + childLetter)

def getSubstringAndMatchScore_trie(userInput, trie, numOfErrorsAllowed, printMode):
	# Recurse through trie until match found...
	output = []
	foundSet = set()
	findInTrie(trie, userInput, len(userInput), numOfErrorsAllowed, 0, output, foundSet, 0, "")

	findInTrieRecurser(trie, userInput, len(userInput), numOfErrorsAllowed, 0, output, foundSet, 0, "")

	if (printMode == False):
		print output
	elif (printMode == "project"):
		if (len(output) > 0):
			print output[0]
		else:
			print "No match."

	return output




# Based off http://en.wikipedia.org/wiki/Levenshtein_distance
def calculateEditDistance(query, word):
	if (query == word):
		return {"word": word, "editDistance": 0}
	if (len(query) == 0):
		return {"word": word, "editDistance": len(word)}
	if (len(word) == 0):
		return {"word": word, "editDistance": len(query)}

	v0 = [None] * (len(word) + 1)
	v1 = [None] * (len(word) + 1)

	for i in range(len(word) + 1):
		v0[i] = i

	for i in range(len(query)):
		v1[0] = i + 1

		for j in range(len(word)):
			cost = 0 if (query[i] == word[j]) else 1
			v1[j + 1] = min([v1[j] + 1, v0[j + 1] + 1, v0[j] + cost])

		for j in range(len(word) + 1):
			v0[j] = v1[j]

	return {"word": word, "editDistance": v1[len(word)]}

def getSubstringAndMatchScore_editDistance(userInput, editDistanceArray, printMode):
	editDistances = []
	uniqueWords = set()
	for word in editDistanceArray:
		if word not in uniqueWords:
			uniqueWords.add(word)
			editDistances.append(calculateEditDistance(userInput, word))
	
	editDistances = sorted(editDistances, key=lambda k: k["editDistance"])

	if (printMode == False):
		if (len(editDistances) > 19):
			print editDistances[:20]
		else:
			print editDistances
		return editDistances
	elif (printMode == "project"):
		if (len(editDistances) > 0):
			print editDistances[0]
		else:
			print "No match."
	else:
		return editDistances

def main():
	# Load in the dictionary, the sample text you will be comparing to.
	# Collect up relevant command line switches.
	# Go into an a while loop that is only exitesad if user enters :q or something
	
	# Possibly switch mode from trie to edit distance if the user enters :trie 
	# or :editDistance or something like that.
	# Do :memory to print out size of data structure currently.
	
	# Continuously read in each input from the command line, and compare to the
	# relevant data structure, printing the output each time.
	# 
	if (len(sys.argv) < 2):
		print '''To use this program, please provide one or two .txt file as arguments.

If two .txt files are supplied, then the first file will be used for 
comparing against each word in the second input file.

If one file is supplied, the first file will be used for comparing 
user entered queries to, to find the match score for the entered query 
when compared with all the 'words' in the provided .txt file.
''' + generalStartupMessage

		sys.exit(0)
	else:
		textFile = sys.argv[1]

		if (len(sys.argv) > 2):
			projectMode = True
			queryFile = sys.argv[2]
			preprocessedQueries = preprocessInputFile(queryFile)
		else:
			projectMode = False

		preprocessedText = preprocessInputFile(textFile)

		#print preprocessedText[:5000]

		trie = buildTrie(preprocessedText)
		editDistanceArray = buildEditDistanceArray(preprocessedText)		

		numberOfErrorsAllowed = "Unset"

		if (projectMode):
			print "How many errors should be allowed when traversing the trie? Enter your amount:"
			
			while (numberOfErrorsAllowed == "Unset"): 
				userInput = str(raw_input())
				try:
					numberOfErrorsAllowed = int(float(userInput))
				except Exception, e:
					print "please enter in an integer value"
			
			print "Number of acceptable errors: " + str(numberOfErrorsAllowed)

			print str(numberOfErrorsAllowed) + " errors allowed."
			print "Processing each query from the query file."
			for word in preprocessedQueries.split(" "):
				print "Query:"
				print word
				print "Trie:"
				getSubstringAndMatchScore_trie(word, trie, numberOfErrorsAllowed, "project")
				print "Edit Distance:"
				getSubstringAndMatchScore_editDistance(word, editDistanceArray, "project")

			print "Sizes of data structures"
			getDataStructureSize(trie, editDistanceArray)

		else:
			numberOfErrorsAllowed = 0
			mode = "trie"
			print generalStartupMessage
			print ""
			print trieModeMessage
			print "Current number of errors for traversing trie: " + str(numberOfErrorsAllowed)
			print "Use :s to set the number of allowable errors"
			userInput = ""
			while True:
				userInput = str(raw_input())
				if (userInput == ":quit" or userInput == ":exit" or userInput == ":q"):
					print "Now exiting. Bye!"
					sys.exit(0)
				elif (userInput == ":trie" or userInput == ":t"):
					mode = "trie"
					print trieModeMessage
				elif (userInput == ":editDistance" or userInput == ":e"):
					mode = "editDistance"
					print editDistanceModeMessage
				elif (userInput == ":memory" or userInput == ":m"):
					getDataStructureSize(trie, editDistanceArray)
				elif (userInput == ":printInput" or userInput == ":p"):
					print preprocessedText
				elif (userInput == ":help" or userInput == ":h"):
					print fullHelpText
				elif (userInput == ":setErrorNum" or userInput == ":s"):
					mode = "setErrorNum"
					print setErrorNumMessage
				elif (userInput == ""):
					print "No input received. Try again."
				elif (mode == "setErrorNum"):
					try:
						numberOfErrorsAllowed = int(float(userInput))
					except Exception, e:
						print "please enter in an integer value"
					else:
						print "Number of acceptable errors: " + str(numberOfErrorsAllowed)
						mode = "trie"
						print trieModeMessage
				elif (mode == "trie"):
					getSubstringAndMatchScore_trie(userInput.lower(), trie, numberOfErrorsAllowed, False)
				elif (mode == "editDistance"):
					getSubstringAndMatchScore_editDistance(userInput.lower(), editDistanceArray, False)
				else: 
					print "Unknown error occurred, exiting."
					sys.exit(1)


if __name__ == "__main__":
	main()


